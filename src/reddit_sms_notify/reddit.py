import logging

import praw
import typer

from reddit_sms_notify import sms

logging.basicConfig(level=logging.INFO)

reddit = praw.Reddit("sms_notify")


def monitor(subreddit: str, search: str) -> None:
    for post in (
        reddit.subreddit(subreddit)
        .stream
        .submissions(skip_existing=True, pause_after=0)
    ):
        if post is None:
            continue
        # TODO: handle regex instead of simple string search
        if search in post.title:
            logging.info(f"found new post: {post.permalink}")
            sms.notify(post.title, f"https://reddit.com{post.permalink}")


if __name__ == "__main__":
    logging.info("Starting typer parsing")
    typer.run(monitor)