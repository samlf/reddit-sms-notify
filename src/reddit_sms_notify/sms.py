import logging
import os

from twilio.rest import Client

logging.basicConfig(level=logging.INFO)

account_sid = os.getenv("TWILIO_SID")
auth_token = os.getenv("TWILIO_TOKEN")
target_number = os.getenv("SMS_TARGET")
sending_number = os.getenv("SMS_SENDER")

client = Client(account_sid, auth_token)

def notify(title: str, url: str) -> None:
    msg_body = f"{title} {url}"
    logging.info(f"Sending SMS: {msg_body}")
    client.messages.create(
        to=target_number,
        from_=sending_number,
        body=msg_body
    )